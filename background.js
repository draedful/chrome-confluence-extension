
/*global chrome*/
chrome.tabs.onActiveChanged.addListener(function(id) {

  chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
    chrome.permissions.contains({
      permissions: ['tabs'],
      origins: [tabs[0].url]
    }, function(result) {
      if (result) {
        chrome.tabs.executeScript(id, {
          file: 'page.js'
        });
      }
    });
  });

});

chrome.commands.onCommand.addListener(function(command) {
  var event = {};
  switch(command) {
    
    case 'style_993300':
      event.commands = ['fill_style_text', 'strikethrough_text'];
      event.color = '993300';
      break;
    
    case 'style_339966':
      event.command = 'fill_style_text';
      event.color = '339966';
      break;

    case 'style_FF9900':
      event.command = 'fill_style_text';
      event.color = 'FF9900';
      break;
    
    default:
      event.command = command;
  }
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
    chrome.tabs.sendMessage(tabs[0].id, event);
  });
});