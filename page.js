;(function() {
  var clearStyleBtn = null,
      strikethroughBtn = null,
      colorBtns = {},
      actions = {
        clear_style: function() {
          if(!clearStyleBtn) {
            clearStyleBtn = document.getElementById('rte-removeformat');
          }
          
          return click(clearStyleBtn);
        },
        fill_style_text: function(data) {
          /*this.clear_style();*/
          if(!colorBtns[data.color]) {
            colorBtns[data.color] = document.querySelector("li a[data-color='"+data.color+"']");
          }
          return click(colorBtns[data.color])
        },
        strikethrough_text: function() {
          if(!strikethroughBtn) {
            strikethroughBtn = document.getElementById('rte-strikethrough');
          }

          return click(strikethroughBtn);
        }
      };

  function click(btn) {
    if(btn) {
      return window.requestAnimationFrame(function() {
        btn.click();
      });
    }
    console.error('button not found');
  }



  chrome.runtime.onMessage.addListener(function(message){

    if(message.command && typeof actions[message.command] === 'function') {
      actions[message.command](message);
    } else if(message.commands) {
      for(var i = 0; i < message.commands.length; i++) {
        if(typeof actions[message.commands[i]] === 'function') {
          actions[message.commands[i]](message);
        }
      }
    }

  });

})();